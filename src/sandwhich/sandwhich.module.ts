import { Module } from '@nestjs/common';
import { SandwhichController } from './sandwhich.controller';
import { SandwhichService } from './sandwhich.service';
import { PrismaModule } from 'src/shared/prisma/prisma.module';

@Module({
  imports: [PrismaModule],
  controllers: [SandwhichController],
  providers: [SandwhichService]
})
export class SandwhichModule {}
