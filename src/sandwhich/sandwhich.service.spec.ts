import { Test, TestingModule } from '@nestjs/testing';
import { SandwhichService } from './sandwhich.service';

describe('SandwhichService', () => {
  let service: SandwhichService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SandwhichService],
    }).compile();

    service = module.get<SandwhichService>(SandwhichService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
