import { Injectable } from '@nestjs/common';
import { Subject } from 'rxjs';
import { PrismaService } from 'src/shared/prisma/prisma.service';

@Injectable()
export class SandwhichService {
    constructor(private prisma: PrismaService) {}

    createdListeningSubject = new Subject();

    async getAll() {
        return await this.prisma.sandwhich.findMany();
    }

    async create() {
        this.createdListeningSubject.next("new sandwhich created");
    }
}
