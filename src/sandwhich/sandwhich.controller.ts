import { Controller, Get, Post, Sse } from '@nestjs/common';
import { Public } from 'src/auth/public.decorator';
import { SandwhichService } from './sandwhich.service';

@Controller('sandwhich')
export class SandwhichController {
    constructor(private service: SandwhichService) {}

    @Get()
    @Public()
    async getSandwhiches() {
        return await this.service.getAll();
    }

    @Post()
    @Public()
    async createSandwhich() {
        return await this.service.create();
    }

    @Public()
    @Sse("/sse")
    async sse() {
        return this.service.createdListeningSubject;
    }
}
