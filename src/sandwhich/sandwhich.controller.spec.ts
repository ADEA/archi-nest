import { Test, TestingModule } from '@nestjs/testing';
import { SandwhichController } from './sandwhich.controller';

describe('SandwhichController', () => {
  let controller: SandwhichController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SandwhichController],
    }).compile();

    controller = module.get<SandwhichController>(SandwhichController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
